-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 01:50 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `productdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `user_id`, `qty`) VALUES
(147, 0, 0, 1),
(148, 0, 28, 1),
(149, 2, 28, 1),
(150, 2, 28, 1),
(151, 0, 28, 1),
(152, 1, 28, 1),
(153, 1, 28, 1),
(154, 1, 28, 1),
(155, 3, 0, 1),
(156, 4, 30, 1),
(157, 3, 30, 1),
(158, 5, 30, 1),
(159, 7, 30, 1),
(160, 4, 30, 1),
(161, 4, 38, 1),
(162, 7, 30, 1),
(163, 8, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `producttb`
--

CREATE TABLE `producttb` (
  `id` int(11) NOT NULL,
  `product_name` varchar(25) NOT NULL,
  `product_price` float DEFAULT NULL,
  `product_desc` text NOT NULL,
  `product_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `producttb`
--

INSERT INTO `producttb` (`id`, `product_name`, `product_price`, `product_desc`, `product_image`) VALUES
(3, 'Sweet Garden Hatbox', 59, 'Our Sweet Garden Hatbox, designed by our top Florists, featuring blush pink roses with pink lisianthus and gypsophilia makes the perfect gift.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and ', './upload/1.png'),
(4, '2 Stem Pink Orchid Plant', 53, 'The very stylish Pink Phalaenopsis orchid is a contemporary favourite, not only because of its chic, decorative flowers but for its captivating elegance too. This gift duo is a fine example of the Phalaenopsis orchid at its best – an abundance of simply exquisite flowers in all their glory.\r\n\r\nFeaturing a pair of purple duo-stemmed Phalaenopsis orchids planted in a classic vase and makes the perfect gift for that someone special.\r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.\r\n\r\nWe have the best Customer Service i', './upload/5.png'),
(5, '2 Stem White Phalaenopsis', 59, 'This elegant 2 stem white Phalaenopsis Orchid is planted in a classic vase and makes the perfect gift for that someone special. Ornate yet understated, this beautiful orchid is perfect for displaying on a mantelpiece or coffee table.\r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', './upload/6.png'),
(6, 'Sweet Garden Basket', 58, 'Our Sweet Garden Basket, arranged by our top Florists and features blush pink roses and tulips with pink lisianthus and gypsophilia to make a lovely traditional gift. \r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', './upload/2.png'),
(7, 'Vibrant Hatbox', 55, 'Our signature Hat Box lovingly arranged with Yellow Roses, Chrysanthemum, Lisianthus, Solidago, Gerbera, Veronica and Eucalyptus.  \r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', './upload/3.png'),
(8, 'Dozen Red Deluxe Hatbox', 70, 'Nothing says it more than a hatbox of Roses, At Flowers.ie we will select the finest red roses sure to send hearts spinning ??????. Featuring large-headed and spray red roses with greeneries, Hand created by our expert team and presented in gift packaging.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', './upload/8.png'),
(9, 'Lavender Floral Hatbox', 700, 'Lavender Floral Hatbox designed by our top Florists, the perfect gift for any occasion, we have chosen the finest fresh flowers imported from the Netherlands.\r\n\r\nOur flowers are sourced from the finest farms in South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee..', './upload/4.png'),
(10, '100 Purple Rose Heart', 400, '100 of the most exquisite and finest Purple roses are perfectly arranged by one of our artisan florists in one of our heart-shaped boxes have a special feminine charm that they will surely love. Imagine the blissful pleasure of this awakening thought. An ultimate symbol of love and this beautiful bouquet sent to your loved one will reaffirm all the beautiful feelings that both of you share.\r\n\r\nThis is also available in White, Pink or Red. \r\n\r\nOur flowers are sourced from the finest farms in Ireland, South America, Holland, France, Italy and Ethiopia and carry our freshness guarantee.', './upload/7.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(27, 'Drake Ortiz', 'Ria Haley', 'mesy@mailinator.com', 'Pa$$w0rd!', '1234543456', '122 Fabien Parkway', 'Magni neque'),
(28, 'Isaac Oconnor', 'Upton Parsons', 'pyvokos@mailinator.com', 'Pa$$w0rd!', '1234567654', '351 Fabien Lane', 'Deserunt eu'),
(29, 'Isaac Oconnor', 'Upton Parsons', 'pyvokos@mailinator.com', 'Pa$$w0rd!', '1234567654', '351 Fabien Lane', 'Deserunt eu'),
(30, 'Fatimah', 'ali', 'fat@hotmail.com', '1qaz2wsx3edc', '1234567654', 'amraca', 'asdfg'),
(31, 'Fatimah', 'ali', 'fat@hotmail.com', '1qaz2wsx3edc', '1234567654', 'amraca', 'asdfg'),
(32, 'Autumn Pate', 'Kim Hunter', 'ruwa@mailinator.com', 'Pa$$w0rd!', '1234565432', '81 White Oak Court', 'Voluptatem '),
(33, 'Autumn Pate', 'Kim Hunter', 'ruwa@mailinator.com', 'Pa$$w0rd!', '1234565432', '81 White Oak Court', 'Voluptatem '),
(34, 'asdfgh', 'asdfgh', 'asdf@hotmail.com', '1qaz2wsx3edc', '1234543234', 'q', 'wer'),
(35, 'asdfgh', 'asdfgh', 'asdf@hotmail.com', '1qaz2wsx3edc', '1234543234', 'q', 'wer'),
(36, 'Justine Rosa', 'Slade Terry', 'ryryluzicu@mailinator.com', 'Pa$$w0rd!', '1234567898', '807 Oak Freeway', 'Excepturi n'),
(37, 'Justine Rosa', 'Slade Terry', 'ryryluzicu@mailinator.com', 'Pa$$w0rd!', '1234567898', '807 Oak Freeway', 'Excepturi n'),
(38, 'Adara Ramsey', 'Teagan Stephens', 'cesy@mailinator.net', 'Pa$$w0rd!', '1234567898', '782 Clarendon Freeway', 'Voluptate t'),
(39, 'Adara Ramsey', 'Teagan Stephens', 'cesy@mailinator.net', 'Pa$$w0rd!', '1234567898', '782 Clarendon Freeway', 'Voluptate t');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producttb`
--
ALTER TABLE `producttb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `producttb`
--
ALTER TABLE `producttb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
