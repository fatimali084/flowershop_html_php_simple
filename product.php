<?
session_start();
require_once ('php/CreateDb.php');
require_once ('./php/component.php');

if (isset($_POST['add'])){
    /// print_r($_POST['product_id']);
    $P_id =$_POST['product_id'];
    $user_id =$_SESSION["uid"];
    if(isset($_SESSION['cart'])){

       
        // Create new session variable
        $_SESSION['cart'][0] = $item_array;
        print_r($_SESSION['cart']);
    
}



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shopping Flowers</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" />

    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style8.css">
    <style >
        .stylebtn{
            background-color:#D6547A; 
            color:#fff;
        }
        .primary-btn {
    display: inline-block;
    padding: 12px 30px;
    background-color: #D6547A;
    border: none;
    border-radius: 40px;
    color: #FFF;
    text-transform: uppercase;
    font-weight: 700;
    text-align: center;
    -webkit-transition: 0.2s all;
    transition: 0.2s all;
}
    </style>
</head>

<body>


<?php require_once ("php/header.php"); ?>
<br>
<br>
<br>
<br>
<br>
<div class="section main main-raised">

			<div class="container">
				<br>
					
				<div class="row">
					
					<?php 
								

								$product_id = $_GET['productid'];
								
								//$sql = " SELECT * FROM products ";
								$sql = "SELECT `id`, `product_name`, `product_price`, `product_desc`, `product_image` FROM `producttb` WHERE `id`=$product_id";
								if (!$con) {
									die("Connection failed: " . mysqli_connect_error());
								}
								// $result = mysqli_query($con, $sql);
 								$query = mysqli_query($con,$sql);

                				if(mysqli_num_rows($query) > 0){
               					// while ($row = mysqli_fetch_assoc($result)){
                                $row=mysqli_fetch_array($query);
								
									//  GetProduct($row['product_image'],$row['product_name'], $row['product_desc'], $row['product_price'],  $row['id']);
 								 echo '<div class="col-md-5 col-md-push-1">
                                <div id="product-main-img">
                                    <div class="product-preview">
                                        <img src="'.$row['product_image'].'" alt="">
                                    </div>

                                    
                                </div>
                            </div>
                                
                                <div class="col-md-2  col-md-pull-5">
                                
                            </div>';?>
                                    
                                    <?php 
                                    echo '<div class="col-md-5">
                                     		<form action="index.php" method="post">
                                            <div class="product-details">
                                                <h2 class="product-name">'.$row['product_name'].'</h2>
                                                <div>
                                                    
                                                    <a class="review-link" href="#review-form">10 Review(s) | Add your review</a>
                                                </div>
                                                <div>
                                                    <h3 class="product-price">$'.$row['product_price'].'<del class="product-old-price">$990.00</del></h3>
                                                    <span class="product-available">In Stock</span>
                                                </div>
                                                <p>'.$row['product_desc'].'</p>

                                                <div class="add-to-cart">
                                                    
                                                    <div class="btn-group" style="margin-left: 25px; margin-top: 15px">
                                                    <button type="submit" class="btn my-3 stylebtn" name="add">Add to Cart <i class="fas fa-shopping-cart"></i></button>
                                                    <input type="hidden" name="product_id" value="'.$row['id'].'">
                                                    </div>
                                                    
                                                    
                                                </div>


                                            </div></form>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        

                            
                            <div class="section main main-raised">
                                <div class="container">
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            
                                        </div> ';
								
									//}
								} ?>	
						
					
				</div>
              <br>  
			</div>
			
		</div>
	
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>	