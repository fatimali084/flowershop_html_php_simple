<?php

session_start();

require_once ('php/CreateDb.php');
require_once ('./php/component.php');


if (isset($_POST['add'])){
    /// print_r($_POST['product_id']);
    $P_id =$_POST['product_id'];
  
    if (isset($_SESSION["uid"])){
          $user_id =$_SESSION["uid"];
    $sql = "INSERT INTO `cart`
            (`p_id`, `user_id`, `qty`) 
            VALUES ('$P_id','$user_id','1')";
             mysqli_query($con,$sql);
    
    if(isset($_SESSION['cart'])){

        $item_array_id = array_column($_SESSION['cart'], "product_id");
        
            
        if(in_array($_POST['product_id'], $item_array_id)){
            echo "<script>alert('Product is already added in the cart..!')</script>";
            echo "<script>window.location = 'index.php'</script>";
        }else{

            $count = count($_SESSION['cart']);
            $item_array = array(
                'product_id' => $_POST['product_id']
            );
            
            $_SESSION['cart'][$count] = $item_array;
        }

    }else{

        $item_array = array(
                'product_id' => $_POST['product_id']
        );

        // Create new session variable
        $_SESSION['cart'][0] = $item_array;
        print_r($_SESSION['cart']);
    }}else{
        echo "<script>alert('Plesse login ot register ..!')</script>";
            echo "<script>window.location = 'index.php'</script>";
    }
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shopping Flowers</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" />

    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style8.css">
    <style >
        .stylebtn{
            background-color:#D6547A; 
            color:#fff;
        }
        .primary-btn {
    display: inline-block;
    padding: 12px 30px;
    background-color: #D6547A;
    border: none;
    border-radius: 40px;
    color: #FFF;
    text-transform: uppercase;
    font-weight: 700;
    text-align: center;
    -webkit-transition: 0.2s all;
    transition: 0.2s all;
}
    </style>
</head>

<body>


<?php require_once ("php/header.php"); ?>
<br>
<br>
<br>
<br>
<br>
 <div class="main main-raised">
    <div id="hot-deal" class="section mainn mainn-raised">
            
            <div class="container">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="hot-deal">
                            <h2 class="text-uppercase">hot deal this week</h2>
                            <p>New Collection Up to 50% OFF</p>
                            <a class="primary-btn cta-btn " href="index.php">Shop now</a>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    
        <div class="row text-center py-5" style="padding-left: 23px;
    padding-right: 23px;">
            <?php
                $sql = "SELECT * FROM producttb";

                $result = mysqli_query($con, $sql);
 
                if(mysqli_num_rows($result) > 0){
                while ($row = mysqli_fetch_assoc($result)){
                    component($row['product_name'], $row['product_price'], $row['product_image'], $row['id']);
                }}
            ?>
        </div>
</div>





<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
